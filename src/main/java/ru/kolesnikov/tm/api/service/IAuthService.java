package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.enumerated.Role;

public interface IAuthService {

    String getUserId();

    void checkRoles(Role[] roles);

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password);

    void registry(String login, String password, String email);

    void updatePassword(String userId, String newPassword);

    void updateUserFirstName(String userId, String newFirstName);

    void updateUserLastName(String userId, String newLastName);

    void updateUserMiddleName(String userId, String newMiddleName);

    void updateUserEmail(String userId, String newEmail);

}