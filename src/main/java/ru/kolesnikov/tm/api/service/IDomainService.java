package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}