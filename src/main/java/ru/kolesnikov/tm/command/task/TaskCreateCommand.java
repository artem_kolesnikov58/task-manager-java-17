package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}