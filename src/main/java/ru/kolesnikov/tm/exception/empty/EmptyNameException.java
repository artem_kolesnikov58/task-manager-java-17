package ru.kolesnikov.tm.exception.empty;

import ru.kolesnikov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}